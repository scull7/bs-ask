open Jest;

describe("Ask.get", () => {
  testAsync("should successfully extract an HttpBin request", done_ =>
    Ask.get("https://httpbin.org/json")
    |. Ask.send
    |. Ask.mapResponse(res =>
         (
           Ask.Res.statusCode(res),
           Ask.Res.body(res)
           |. HttpBin.Json.parse
           |. HttpBin.Json.slideshow
           |. HttpBin.Slideshow.author,
         )
       )
    |. Ask.run(
         res =>
           res
           |. Ask.Res.body
           |. Expect.expect
           |> Expect.toEqual((200, "Yours Truly"))
           |> done_,
         exn => exn |. Js.String.make |. fail |. done_,
       )
  );

  testPromise("should return a promise of the expected result", () =>
    Ask.get("https://httpbin.org/json")
    |. Ask.send
    |. Ask.mapResponse(res =>
         res
         |. Ask.Res.body
         |. HttpBin.Json.parse
         |. HttpBin.Json.slideshow
         |. HttpBin.Slideshow.author
       )
    |. Ask.toPromise
    |> Js.Promise.then_(res =>
         res
         |> Ask.Res.body
         |> Expect.expect
         |> Expect.toBe("Yours Truly")
         |> Js.Promise.resolve
       )
  );
});
