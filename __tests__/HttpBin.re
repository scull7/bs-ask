module Slideshow = {
  module Slide = {
    [@bs.deriving abstract]
    type t = {
      title: string,
      [@bs.as "type"]
      slide_type: string,
      [@bs.optional]
      items: array(string),
    };
  };

  [@bs.deriving abstract]
  type t = {
    author: string,
    date: string,
    title: string,
    slides: array(Slide.t),
  };
};

module Json = {
  [@bs.deriving abstract]
  type t = {slideshow: Slideshow.t};

  [@bs.scope "JSON"] [@bs.val] external parse : string => t = "";
};
