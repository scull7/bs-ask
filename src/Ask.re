module Buffer = Hyperquest.Buffer;
module Hreq = Hyperquest;
module Hres = Hyperquest.Msg;

module Req = Ask_request;

module Res = {
  type t('a) = {
    response: Hres.t,
    body: 'a,
  };

  let make = (response, body) => {response, body};

  let body = t => t.body;

  let statusCode = t => t.response |. Hres.statusCode;

  let map = (t, f) => f(t) |> make(t.response);
};

type callback('a) = Belt.Result.t(Res.t('a), exn) => unit;
type handler('a) = callback('a) => unit;

type request =
  | Request(Req.t);

type response('a) =
  | Response(handler('a));

let get = uri => Req.make(`get, uri) |. Request;

let onRes = (finish, res) => {
  let data = ref("");

  res
  |. Hres.on(`data(b => data := data^ ++ Buffer.toString(b)))
  |. Hres.on(`error(exn => exn |. Belt.Result.Error |. finish))
  |. Hres.on(`end_(_ => Res.make(res, data^) |. Belt.Result.Ok |. finish))
  |. ignore;
};

let send = (Request(req)) =>
  req
  |. Req.compile
  |. (
    (req, finish) => req |. Hreq.on(`response(finish |. onRes)) |. ignore
  )
  |. Response;

let return = (res, done_) => res |. Belt.Result.Ok |. done_;

let flatMap = (Response(deferred), f) =>
  Response(
    defer =>
      deferred(
        fun
        | Belt.Result.Ok(res) => f(res) |. (x => x(defer))
        | Belt.Result.Error(exn) => exn |. Belt.Result.Error |. defer,
      ),
  );

let apply = (Response(applicative), Response(deferred)) =>
  flatMap(Response(applicative), ({body: f, _}) =>
    flatMap(Response(deferred), res => res |. f |. return)
    |. ((Response(handler)) => handler)
  );

let map = (Response(deferred), f) =>
  flatMap(Response(deferred), res => res |. f |. return);

let mapResponse = (Response(deferred), f) =>
  map(Response(deferred), res => res |. Res.map(f));

let run = (Response(deferred), success, error) =>
  deferred(
    fun
    | Belt.Result.Ok(res) => res |. success
    | Belt.Result.Error(exn) => exn |. error,
  );

let toPromise = (Response(deferred)) =>
  Js.Promise.make((~resolve, ~reject) =>
    run(Response(deferred), res => resolve(. res), exn => reject(. exn))
  );
