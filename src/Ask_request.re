module Hreq = Hyperquest;

module Query = {
  type t = option(string);

  let make = () => None;
};

module Url = {
  type t = string;

  let compile = t => t;
};

module Header = {
  type t = option(string);

  let make = () => None;
};

module Get = {
  type t = {
    url: Url.t,
    header: Header.t,
    query: Query.t,
  };

  let compile = t => Hreq.get(~uri=t.url |. Url.compile, ());

  let make = url => {url, header: Header.make(), query: Query.make()};
};

type t = [ | `GET(Get.t)];

let compile =
  fun
  | `GET(get) => Get.compile(get);

let make = (method, url) =>
  switch (method) {
  | `get => `GET(url |. Get.make)
  };
