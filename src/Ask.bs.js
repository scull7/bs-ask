// Generated by BUCKLESCRIPT VERSION 3.1.5, PLEASE EDIT WITH CARE
'use strict';

var Block = require("bs-platform/lib/js/block.js");
var Curry = require("bs-platform/lib/js/curry.js");
var Hyperquest = require("bs-hyperquest/src/Hyperquest.bs.js");
var Ask_request = require("./Ask_request.bs.js");

function make(response, body) {
  return /* record */[
          /* response */response,
          /* body */body
        ];
}

function body(t) {
  return t[/* body */1];
}

function statusCode(t) {
  return Hyperquest.Msg[/* statusCode */6](t[/* response */0]);
}

function map(t, f) {
  return /* record */[
          /* response */t[/* response */0],
          /* body */Curry._1(f, t)
        ];
}

var Res = /* module */[
  /* make */make,
  /* body */body,
  /* statusCode */statusCode,
  /* map */map
];

function get(uri) {
  return /* Request */[Ask_request.make(/* get */5144726, uri)];
}

function send(param) {
  var partial_arg = Ask_request.compile(param[0]);
  return /* Response */[(function (param) {
              Hyperquest.on(partial_arg, /* `response */[
                    571256449,
                    (function (param$1) {
                        var finish = param;
                        var res = param$1;
                        var data = [""];
                        Hyperquest.Msg[/* on */0](Hyperquest.Msg[/* on */0](Hyperquest.Msg[/* on */0](res, /* `data */[
                                      -1033677270,
                                      (function (b) {
                                          data[0] = data[0] + Hyperquest.$$Buffer[/* toString */0](b);
                                          return /* () */0;
                                        })
                                    ]), /* `error */[
                                  -215364664,
                                  (function (exn) {
                                      return Curry._1(finish, /* Error */Block.__(1, [exn]));
                                    })
                                ]), /* `end_ */[
                              -1021944796,
                              (function () {
                                  return Curry._1(finish, /* Ok */Block.__(0, [/* record */[
                                                  /* response */res,
                                                  /* body */data[0]
                                                ]]));
                                })
                            ]);
                        return /* () */0;
                      })
                  ]);
              return /* () */0;
            })];
}

function $$return(res, done_) {
  return Curry._1(done_, /* Ok */Block.__(0, [res]));
}

function flatMap(param, f) {
  var deferred = param[0];
  return /* Response */[(function (defer) {
              return Curry._1(deferred, (function (param) {
                            if (param.tag) {
                              return Curry._1(defer, /* Error */Block.__(1, [param[0]]));
                            } else {
                              return Curry._2(f, param[0], defer);
                            }
                          }));
            })];
}

function apply(param, param$1) {
  var deferred = param$1[0];
  return flatMap(/* Response */[param[0]], (function (param) {
                var f = param[/* body */1];
                return flatMap(/* Response */[deferred], (function (res) {
                                var partial_arg = Curry._1(f, res);
                                return (function (param) {
                                    return Curry._1(param, /* Ok */Block.__(0, [partial_arg]));
                                  });
                              }))[0];
              }));
}

function map$1(param, f) {
  return flatMap(/* Response */[param[0]], (function (res) {
                var partial_arg = Curry._1(f, res);
                return (function (param) {
                    return Curry._1(param, /* Ok */Block.__(0, [partial_arg]));
                  });
              }));
}

function mapResponse(param, f) {
  return map$1(/* Response */[param[0]], (function (res) {
                return map(res, f);
              }));
}

function run(param, success, error) {
  return Curry._1(param[0], (function (param) {
                if (param.tag) {
                  return Curry._1(error, param[0]);
                } else {
                  return Curry._1(success, param[0]);
                }
              }));
}

function toPromise(param) {
  var deferred = param[0];
  return new Promise((function (resolve, reject) {
                return run(/* Response */[deferred], (function (res) {
                              return resolve(res);
                            }), (function (exn) {
                              return reject(exn);
                            }));
              }));
}

var Req = 0;

exports.Req = Req;
exports.Res = Res;
exports.get = get;
exports.send = send;
exports.$$return = $$return;
exports.flatMap = flatMap;
exports.apply = apply;
exports.map = map$1;
exports.mapResponse = mapResponse;
exports.run = run;
exports.toPromise = toPromise;
/* Hyperquest Not a pure module */
