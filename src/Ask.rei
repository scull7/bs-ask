module Req = Ask_request;

module Res: {
  type t('a);

  let make: (Hyperquest.Msg.t, 'a) => t('a);

  let body: t('a) => 'a;

  let statusCode: t('a) => int;

  let map: (t('a), t('a) => 'b) => t('b);
};

type handler('a) = (Belt.Result.t(Res.t('a), exn) => unit) => unit;

type request =
  | Request(Req.t);
type response('a) =
  | Response(handler('a));

let get: string => request;

let send: request => response(string);

/**
 * I don't think that this truly follows the monadic laws but,
 * I'm not sure how to wrap this in the outer `Response` variant
 * constructor and make it ergonomic to use.
 */
let return: Res.t('a) => handler('a);

let flatMap: (response('a), Res.t('a) => handler('b)) => response('b);

let apply:
  (response(Res.t('a) => Res.t('b)), response('a)) => response('b);

let map: (response('a), Res.t('a) => Res.t('b)) => response('b);

let mapResponse: (response('a), Res.t('a) => 'b) => response('b);

let run: (response('a), Res.t('a) => unit, exn => unit) => unit;

/* @TODO - implement toFuture */
/* let toFuture: response('a) => Future(Belt.Result.t('a, exn)); */
let toPromise: response('a) => Js.Promise.t(Res.t('a));
