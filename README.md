# bs-ask

My attempt at writing a more idiomatic ReasonML HTTP client.

## Usage

```reason
module HttpBin = {
  module Slideshow = {
    module Slide = {
      [@bs.deriving abstract]
      type t = {
        title: string,
        [@bs.as "type"] slide_type: string,
        [@bs.optional] items: array(string),
      }
    };

    [@bs.deriving abstract]
    type t = {
      author: string,
      date: string,
      title: string,
      slide: array(Slide.t),
    };
  };
};
```

### Get Request
```reason
Ask.get("http://httpbin.org/json")
|. Ask.send
|. Ask.receive((request, response, body) => {
    /* Should log
    let status = Ask.Response.statusCode(response);
    let author = body |. HttpBin.Slideshow.author;

    Js.log3("Response: ", status, author);
  })
```
